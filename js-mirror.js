
/*--- N A V---*/

const navSlide = () => {
    const burger = document.querySelector(".burger");
    const nav = document.querySelector(".nav-links");
    const navLinks = document.querySelectorAll(".nav-links a");
  
    burger.addEventListener("click", () => {
      nav.classList.toggle("nav-active");
  
      navLinks.forEach((link, index) => {
        if (link.style.animation) {
          link.style.animation = "";
        } else {
          link.style.animation = `navLinkFade 0.5s ease forwards ${
            index / 7 + 0.5
          }s `;
        }
      });
      burger.classList.toggle("toggle");
    });
    //
  };
  
  navSlide();

  /*--- C A R R I T O  N A V---*/

  const carritoNavSlide = () => {
    const carrito = document.querySelector(".carrito");
    let carritoNav = document.querySelector(".carrito-nav-contenedor");

    carrito.addEventListener("click", () =>{
      carritoNav.classList.toggle("carrito-nav-contenedor-active");
      
    });
    
  };
  carritoNavSlide();

const cerrarCarritoNavSlide = () => {
    const cerrar = document.querySelector(".cerrar");
    let carritoNav = document.querySelector(".carrito-nav-contenedor");


    cerrar.addEventListener("click", () =>{
      carritoNav.classList.toggle("carrito-nav-contenedor-active");
    });
    
  };
  cerrarCarritoNavSlide();

   /*--- P O P - U P ---*/

   const popupWindow = () => {
    const popup = document.querySelectorAll(".img-popup");
    let popupWindow = document.querySelector(".pop-up-w");

    popup.forEach(popup => {
    popup.addEventListener("click", () =>{
      popupWindow.classList.toggle("pop-up-w-active");
      popupWindow.scrollTop;
      
    });
  });
  };
  popupWindow();

  const cerrarpopupWindow = () => {
    const cerrar = document.querySelector(".cerrarpopup");
    let popupWindow = document.querySelector(".pop-up-w");
    
    cerrar.addEventListener("click", () =>{
      popupWindow.classList.toggle("pop-up-w-active");
      
    });
    
  };
  cerrarpopupWindow(); 